# GrayRose: A Serverless Room Occupancy Detector


## Required Hardware
- Raspberry Pi
- External mouse and keyboard (To set up raspberry pi)

## Required Software
- A POSIX compliant OS (e.g. Ubuntu, MacOS, etc...)
- [Python 3](https://www.python.org/downloads/)

## Setup


### AWS Security Credentials
- Install the aws cli
```sh
pip3 install awscli --user
```
- Configure the aws credentials with the account credentials you have been given
```sh
aws configure --profile grey-rose-{name}
```

### Serverless
This project uses the [serverless](https://serverless.com/) framework to deploy infrastructure as code.
Clone this repo. Go to the root of the project and issue the following commands to install serverless and the required plugins.
```sh
curl -o- -L https://slss.io/install | bash
sls plugin install --name serverless-python-requirements
```
The plugin serverless-python-requirements allows serverless to build the package inside a Docker image that replicates the AWS Lambda runtime environment, giving us platform independence for development.

To spin up the AWS resources, type
```sh
serverless deploy --aws-profile=grey-rose-{name}
```

