from setuptools import setup

setup(
   name='grey_rose',
   version='1.0',
   description='A slack bot that monitors the occupancy of meeting rooms for quickly checking if they are free.',
   author='James Tait & Tom Noble',
   author_email='james.tait@bjss.com & tom.noble@bjss.com',
   packages=['src/grey_rose']  #same as name
)