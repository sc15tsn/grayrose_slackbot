# from random import choice

# from grey_rose.bot_commands.commands import Command

# class MockSlackClient:
    
#     def __init__(self, token, channels):
#         self.channels = channels

#     def chat_postMessage(self, **kwargs):
#         return kwargs

#     def conversations_list(self):
#         return self.channels

# class MockOfficeMonitor:

#     def __init__(self):
#         self.rooms = [
#             {'name': 'Room 1', 'is_empty': True},
#             {'name': 'Room 2', 'is_empty': False},
#             {'name': 'Room 3', 'is_empty': True},
#             {'name': 'Room 4', 'is_empty': True},
#             {'name': 'Room 5', 'is_empty': False}
#         ]

#     def get_rooms(self):
#         return self.rooms


# class UniformOfficeMonitor:

#     def __init__(self):
#         self.rooms = [
#             {'name': 'Room 1'},
#             {'name': 'Room 2'},
#             {'name': 'Room 3'},
#             {'name': 'Room 4'},
#             {'name': 'Room 5'}
#         ]

#     def get_rooms(self):
#         for room in self.rooms:
#             room['is_empty'] = choice([True, False])
#         return self.rooms

# class MockCommand(Command):
    
#     def __init__(self, name, description):
#         super().__init__(name, description)
#         self.ran = False

#     def run(self):
#         self.ran = True
