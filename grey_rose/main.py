"""
Slack chat-bot Lambda handler.
"""
from grey_rose.slack.event import SlackEventFactory
from grey_rose.slack.request import SlackRequest
from grey_rose.slack.slack_http_response import SlackHTTPResponse
from grey_rose.slack.event_handlers import handle_slack_event


def lambda_handler(slack_request, context):
    """Handle an incoming HTTP request from a Slack chat-bot.
    """

    slack_request = SlackRequest(slack_request)
    slack_request_body = slack_request.get_slack_request_body()
    print(slack_request_body)

    if slack_request.is_command():
        response_body = ''
        slack_event = SlackEventFactory.build_slack_event(
            slack_request_body
        )
        handle_slack_event(slack_event) 
    elif slack_request.is_url_verification_request():
        # Slack expects you to return the challenge token in order to verify
        # the application
        response_body = slack_request_body['challenge']
    elif slack_request.is_bot_message() or slack_request.is_message_changed():
        response_body = 'Ignoring event'
    elif slack_request.is_block_action():
        response_body = 'Received and working on the message!'
        slack_event = SlackEventFactory.build_slack_event(
            slack_request_body
        )
        handle_slack_event(slack_event)
    elif slack_request.is_app_mention():
        response_body = 'Received and working on the message!'
        slack_event = SlackEventFactory.build_slack_event(
            slack_request_body['event']
        )
        handle_slack_event(slack_event)
    elif slack_request.is_direct_message():
        response_body = 'Received and working on the message!'
        slack_event = SlackEventFactory.build_slack_event(
            slack_request_body['event']
        )
        handle_slack_event(slack_event)
    else:
        response_body = 'Unrecognised event'
    return SlackHTTPResponse(200, response_body).to_json()
