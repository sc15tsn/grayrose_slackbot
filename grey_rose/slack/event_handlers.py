from grey_rose.slack.event import (
    AppMentionEvent,
    BlockActionEvent,
    MessageEvent,
    CommandEvent
)
from grey_rose.slack.slackbot import (
    post_message,
    post_command_response,
    get_room_statuses,
    update_message
)
from grey_rose.slack.messages import Message, MessageComponents


def handle_slack_event(slack_event):
    if isinstance(slack_event, AppMentionEvent):
        handle_app_mention_event(slack_event)
    elif isinstance(slack_event, BlockActionEvent):
        handle_block_action_event(slack_event)
    elif isinstance(slack_event, MessageEvent):
        handle_message_event(slack_event)
    elif isinstance(slack_event,CommandEvent):
        handle_command_event(slack_event)

def handle_app_mention_event(slack_event):
    channel_id = slack_event.get_channel_id()
    room_statuses = get_room_statuses()
    message = build_room_status_message(room_statuses)
    post_message(channel_id, message)


def handle_block_action_event(slack_event):
    channel_id = slack_event.get_channel_id()
    action_message_timestamp = slack_event.get_action_timestamp()
    room_statuses = get_room_statuses()
    new_blocks = build_room_status_message(room_statuses).message['blocks']
    update_message(
        channel=channel_id,
        message_timestamp=action_message_timestamp,
        new_message_blocks=new_blocks
    )

def handle_message_event(slack_event):
    channel_id = slack_event.get_channel_id()
    room_statuses = get_room_statuses()
    message = build_room_status_message(room_statuses)
    post_message(channel_id, message)

def handle_command_event(slack_event):
    response_url = slack_event.get_response_url()
    room_statuses = get_room_statuses()
    message = build_room_status_message(room_statuses)
    post_command_response(response_url, message)

def build_room_status_message(room_statuses):
    if room_statuses is []:
        return Message([
            MessageComponents.markdown(
                'There are currently no rooms to display'
            )
        ])

    text_and_button_component = MessageComponents.markdown(
        'Hello! Here are the statuses of the meeting rooms:'
    )
    text_and_button_component.add_button(
        text=':refresh_icon:',
        value='refresh',
        allow_emoji=True
    )
    room_status_components = [
        MessageComponents.room(
            room_status
        ) for room_status in room_statuses
    ]
    message = Message(
        [
            text_and_button_component,
            MessageComponents.divider()
        ] + room_status_components
        + [MessageComponents.divider()]
    )
    return message
