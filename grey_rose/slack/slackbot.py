import os
import requests

from slack import WebClient

from grey_rose.repositories import RoomStatusRepositories


SLACK_WEB_CLIENT = WebClient(os.environ['BOT_TOKEN'])

ROOM_STATUS_REPOSITORY = RoomStatusRepositories.dynamo_db(
    table_name='grey-rose-rooms-status',
    region='eu-west-2'
)


def post_message(channel, message, slack_web_client=SLACK_WEB_CLIENT):
    message = message.message
    message.update({
        'channel': channel
    })
    print(message)
    return slack_web_client.chat_postMessage(**message)


def post_command_response(response_url, message):
    message = message.message
    message.update({"response_type": "in_channel"})
    response = requests.post(response_url, str(message))
    return response


def update_message(
        channel,
        message_timestamp,
        new_message_blocks,
        slack_web_client=SLACK_WEB_CLIENT
):
    kwargs = {
        'as_user': True
    }
    return slack_web_client.chat_update(
        channel=channel,
        ts=message_timestamp,
        blocks=new_message_blocks,
        kwargs=kwargs
    )


def get_room_statuses(room_status_repository=ROOM_STATUS_REPOSITORY):
    return room_status_repository.get_rooms()
