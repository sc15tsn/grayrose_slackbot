import json
from urllib.parse import unquote, parse_qsl


class SlackRequest:

    def __init__(self, slack_request):
        body = unquote(slack_request['body'])
        if body.startswith('payload='):
            body = body.strip('payload=')
            body = json.loads(body)
        elif body.startswith('token='):
            body = unquote(body)
            body = dict(parse_qsl(body))
        else:
            body = json.loads(body)
        self.slack_request_body = body

    def is_command(self):
        return 'type' not in self.slack_request_body and\
             'command' in self.slack_request_body

    def is_url_verification_request(self):
        return self.slack_request_body['type'] == 'url_verification'

    def is_block_action(self):
        return self.slack_request_body['type'] == 'block_actions'

    def is_app_mention(self):
        return self.contains_slack_event and self.slack_request_body['event']['type'] == 'app_mention'

    def is_message(self):
        return self.contains_slack_event() and self.slack_request_body['event']['type'] == 'message'

    def is_direct_message(self):
        return self.is_message() and self.slack_request_body['event']['channel_type'] == 'im'

    def is_bot_message(self):
        return self.is_message() and 'bot_id' in self.slack_request_body['event']

    def is_message_changed(self):
        return self.is_message() and 'subtype' in self.slack_request_body['event'] and \
               self.slack_request_body['event']['subtype'] == 'message_changed'

    def contains_slack_event(self):
        return 'event' in self.slack_request_body

    def get_slack_request_body(self):
        return self.slack_request_body
