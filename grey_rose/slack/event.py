import dependency_injector.containers as containers
import dependency_injector.providers as providers


class SlackEvent:
    """Base class for the Slack event.

    Most requests from slack contain an event object. These have different
    properties and have different responses from the application.
    """

    def __init__(self, slack_event):
        self.event = slack_event

    def __str__(self):
        return self.event.__str__()


class AppMentionEvent(SlackEvent):
    """Slack event when the bot is mentioned.

    When the bot is mentioned by a user, a request is sent to the application
    using this event type. The text is parsed and the action carried out.
    """

    def get_channel_id(self):
        return self.event['channel']

    def get_message_content(self):
        return self.event['text']

    def get_message_content_without_bot_id(self):
        full_content_as_words_list = self.get_message_content().split(' ')
        return (' ').join(full_content_as_words_list[1:])


class BlockActionEvent(SlackEvent):
    """Slack event when a block action occurs.

    When a block action occurs, a request is sent to the application
    with this event type. An example is clicking the refresh button.
    """

    def get_channel_id(self):
        return self.event['channel']['id']

    def get_action_timestamp(self):
        return self.event['message']['ts']


class MessageEvent(SlackEvent):

    def get_channel_id(self):
        return self.event['channel']

    def get_message_content(self):
        return self.event['text']


class CommandEvent(SlackEvent):

    def get_channel_id(self):
        return self.event['channel_id']

    def get_response_url(self):
        return self.event['response_url']


class SlackEventFactory:

    @staticmethod
    def build_slack_event(slack_event):
        if 'type' in slack_event:
            if slack_event['type'] == 'app_mention':
                return SlackEvents.app_mention(slack_event)
            elif slack_event['type'] == 'block_actions':
                return SlackEvents.block_action(slack_event)
            elif slack_event['type'] == 'message':
                return SlackEvents.message(slack_event)
        else:
            if 'command' in slack_event:
                return SlackEvents.command(slack_event)


class SlackEvents(containers.DeclarativeContainer):
    """Inversion of control container of slack event providers."""

    app_mention = providers.Factory(AppMentionEvent)
    block_action = providers.Factory(BlockActionEvent)
    message = providers.Factory(MessageEvent)
    command = providers.Factory(CommandEvent)
