class SlackHTTPResponse:
    """ Used to send http responses back to the slackbot"""

    def __init__(self, status_code, message_body):
        self.status_code = status_code
        self.headers = {'Content-Type': 'application/json'}
        self.message_body = message_body

    def to_json(self):
        return {
            'statusCode': self.status_code,
            'headers': self.headers,
            'body': self.message_body
        }
