from abc import abstractmethod

from dependency_injector import containers
from dependency_injector import providers


class Message:
    """A wrapper for objects to send back to the Slack channel.

    A Message is made up of many MessageComponent like objects. They are stored
    as a list in the message field which is the field used by slack.
    """

    def __init__(self, message_components):
        blocks = [component.block for component in message_components]
        self.message = {
            'as_user': True,
            'ts': "",
            'blocks': blocks
        }


class MessageComponent:
    """The building blocks of the Message class (wraps slack blocks).

    Messages are built up out of MessageComponents and are collected in a list
    before being sent to the slack channel.
    """

    def __init__(self, block_type, **kwargs):
        text = self.build_content(**kwargs)
        if text is not None:
            self.block = {
                'type': block_type,
                'text': self.build_content(**kwargs)
            }
        else:
            self.block = {
                'type': block_type,
            }

    @abstractmethod
    def build_content(self, **kwargs):
        """Override this method to define how the content of the component is
        structured. Slack expects a 'dict' type.
        """

    def add_button(self, text, value, allow_emoji=False):
        self.block.update({
            'accessory': {
                'type': 'button',
                'text': {
                    'type': 'plain_text',
                    'text': text,
                    'emoji': allow_emoji
                },
                'value': value
            }
        })


class MarkdownComponent(MessageComponent):
    """Component for building a markdown message.

    Slack has the ability to post messages in a markdown format. This component
    allows you to fill a markdown component with some markdown code that will
    be formatted correctly by slack.
    """

    def __init__(self, text):
        kwargs = {'text': text}
        super().__init__(block_type='section', **kwargs)

    def build_content(self, **kwargs):
        return {
            'type': 'mrkdwn',
            'text': kwargs['text']
        }


class RoomStatusComponent(MessageComponent):
    """Component for displaying the status of one room.

    This component formats a message to display the status of a single room.
    """

    def __init__(self, room):
        kwargs = {'room': room}
        super().__init__(block_type='section', **kwargs)

    def build_content(self, **kwargs):
        room = kwargs['room']
        indent_string = '>>>'
        bold_string = '*'
        status_icon = ':blue_tick:' if room["is_empty"] == 'true'\
            else ':red_cross:'

        indent_and_icon = f'{indent_string}{status_icon}'
        bold_room_name = f'{bold_string}{room["room_name"]}{bold_string}'
        last_updated = room["last_updated"]
        text = f'{indent_and_icon}   {bold_room_name}   last update: {last_updated}'

        return {
            'type': 'mrkdwn',
            'text': text
        }


class Divider(MessageComponent):
    """A simple divider component."""

    def __init__(self):
        super().__init__(block_type='divider')

    def build_content(self, **kwargs):
        pass


class MessageComponents(containers.DeclarativeContainer):
    """Inversion of control container of message component providers."""

    markdown = providers.Factory(MarkdownComponent)
    room = providers.Factory(RoomStatusComponent)
    divider = providers.Factory(Divider)
