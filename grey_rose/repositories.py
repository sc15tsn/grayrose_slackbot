from abc import abstractmethod
import boto3

from dependency_injector import containers
from dependency_injector import providers


class RoomStatusRepository:

    @abstractmethod
    def get_rooms(self):
        pass


class DynamoDBRoomStatusRepository(RoomStatusRepository):

    def __init__(self, table_name, region):
        session = boto3.session.Session()
        database = session.resource('dynamodb', region_name=region)
        self.table = database.Table(table_name)

    def get_rooms(self):
        rooms = self.table.scan()['Items']
        return rooms


class RoomStatusRepositories(containers.DeclarativeContainer):
    """Inversion of control container of room status repository providers."""

    dynamo_db = providers.Factory(DynamoDBRoomStatusRepository)
