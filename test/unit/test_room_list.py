import pytest

from room_list import RoomList


class TestRoomList:

    @pytest.fixture
    def example_room_list_dict(self):
        return [
            {'name': 'Room 1', 'is_empty': True},
            {'name': 'Room 2', 'is_empty': False}
        ]

    @pytest.fixture
    def example_room_list(self, example_room_list_dict):
        return RoomList(example_room_list_dict)

    @pytest.fixture
    def example_room_1_string(self):
        return ':green_circle:   Room 1'

    @pytest.fixture
    def example_room_1_bold_string(self):
        return ':green_circle:   *Room 1*'

    @pytest.fixture
    def example_room_1_indent_string(self):
        return '>>>:green_circle:   Room 1'

    @pytest.fixture
    def example_room_1_indent_bold_string(self):
        return '>>>:green_circle:   *Room 1*'

    @pytest.fixture
    def example_room_2_string(self):
        return ':red_circle:   Room 2'

    @pytest.fixture
    def example_room_2_indent_bold_string(self):
        return '>>>:red_circle:   *Room 2*'

    @pytest.fixture
    def refresh_button(self):
        return {
            'type': 'button',
            'text': {
                'type': 'plain_text',
                'text': ':refresh_icon:',
                'emoji': True
            },
            'value': 'refresh',
        }

    @pytest.fixture
    def header_text(self, refresh_button):
        return {
            'type': 'mrkdwn',
            'text': 'Here are the statuses of the meeting rooms:'
        }

    @pytest.fixture
    def example_room_1_block(self, example_room_1_indent_bold_string):
        return {
            'type': 'mrkdwn',
            'text': example_room_1_indent_bold_string
        }

    @pytest.fixture
    def example_room_2_block(self, example_room_2_indent_bold_string):
        return {
            'type': 'mrkdwn',
            'text': example_room_2_indent_bold_string
        }

    @pytest.fixture
    def example_room_list_blocks(self, header_text, refresh_button, example_room_1_block, example_room_2_block):
        return [
            {
                'type': 'section',
                'text': header_text,
                'accessory': refresh_button
            },
            {'type': 'divider'},
            {
                'type': 'section',
                'text': example_room_1_block
            },
            {
                'type': 'section',
                'text': example_room_2_block
            },
            {'type': 'divider'}
        ]

    def test_format_room(self, example_room_list, example_room_1_string, example_room_2_string,
                         example_room_1_bold_string, example_room_1_indent_string, example_room_1_indent_bold_string):
        assert example_room_list.format_room(example_room_list.room_list[0]) == example_room_1_string
        assert example_room_list.format_room(example_room_list.room_list[1]) == example_room_2_string
        assert example_room_list.format_room(example_room_list.room_list[0], bold=True) == example_room_1_bold_string
        assert example_room_list.format_room(example_room_list.room_list[0], indent=True) == example_room_1_indent_string
        assert example_room_list.format_room(example_room_list.room_list[0], bold=True, indent=True) == example_room_1_indent_bold_string

    def test_to_slack_blocks(self, example_room_list, example_room_list_blocks):
        assert RoomList(room_list=[]).to_slack_blocks() == []
        assert example_room_list.to_slack_blocks() == example_room_list_blocks