import pytest

from event_handlers import ValidationEventHandler, BotMessageEventHandler, UserMessageEventHandler
from mocks import MockSlackClient, MockCommand
from command_interpreter import SlackCommandInterpreter
from slack_helpers import MessageSender

class TestValidationEventHandler:

    @pytest.fixture
    def example_validation_event(self):
        return {
            'token': 'Jhj5dZrVaK7ZwHHjRyZWjbDl',
            'challenge': '3eZbrw1aBm2rZgRNFdxV2595E9CY3gmdALWMmHkvFXO7tYXAYM8P',
            'type': 'url_verification',
            'body': 'test'
        }

    @pytest.fixture
    def example_non_validation_event(self):
        return {
            'a': 'b',
            'c': 'd',
            'type': 'other',
            'body': 'test2',
        }

    @pytest.fixture
    def example_validation_event_response(self, example_validation_event):
        return {
            'statusCode': 200,
            'headers': {'Content-Type': 'application/json'},
            'body': 'test'
        }

    def test_is_this_type(self, example_validation_event, example_non_validation_event):
        handler = ValidationEventHandler()
        assert handler.is_this_type(example_validation_event) is True
        assert handler.is_this_type(example_non_validation_event) is False

    def test_handle(self, example_validation_event, example_validation_event_response):
        handler = ValidationEventHandler()
        assert handler.handle(example_validation_event) == example_validation_event_response


class TestBotMessageEventHandler:

    @pytest.fixture
    def example_bot_message_event(self):
        return {
            'token': 'test_token',
            'team_id': 'test_id',
            'api_app_id': 'test_id',
            'event': {
                'bot_id': 'test_id',
                'type': 'message',
                'text': "test_message",
                'user': 'test_user',
                'ts': '1574953111.001100',
                'team': 'test_team',
                'bot_profile': "test_profile",
                'blocks': [],
                'channel': 'test_channel',
                'event_ts': '1574953111.001100',
                'channel_type': 'channel'
            },
            'type': 'event_callback',
            'event_id': 'test_id',
            'event_time': 1574953111,
            'authed_users': ['test_user']
        }

    @pytest.fixture
    def example_user_message_event(self):
        return {
            'token': 'test_token',
            'team_id': 'test_id',
            'api_app_id': 'test_id',
            'event': {
                'client_msg_id': 'test_id',
                'type': 'message',
                'text': 'test_message',
                'user': 'test_user',
                'ts': '1574953107.001000',
                'team': 'test_team',
                'blocks': [],
                'channel': 'test_channel',
                'event_ts': '1574953107.001000',
                'channel_type': 'channel'
            },
            'type': 'event_callback',
            'event_id': 'test_id',
            'event_time': 1574953107,
            'authed_users': ['test_user']
        }

    def test_is_this_type(self, example_bot_message_event, example_user_message_event):
        handler = BotMessageEventHandler()
        assert handler.is_this_type(example_bot_message_event) is True
        assert handler.is_this_type(example_user_message_event) is False

    def test_handle(self, example_bot_message_event):
        handler = BotMessageEventHandler()
        assert handler.handle(example_bot_message_event) == '200 OK'


class TestUserMessageEventHandler:

    @pytest.fixture
    def example_user_message_event(self):
        return {
            'token': 'test_token',
            'team_id': 'test_id',
            'api_app_id': 'test_id',
            'event': {
                'client_msg_id': 'test_id',
                'type': 'message',
                'text': 'test_message',
                'user': 'test_user',
                'ts': '1574953107.001000',
                'team': 'test_team',
                'blocks': [],
                'channel': 'test_channel',
                'event_ts': '1574953107.001000',
                'channel_type': 'channel'
            },
            'type': 'event_callback',
            'event_id': 'test_id',
            'event_time': 1574953107,
            'authed_users': ['test_user']
        }

    @pytest.fixture
    def example_bot_message_event(self):
        return {
            'token': 'test_token',
            'team_id': 'test_id',
            'api_app_id': 'test_id',
            'event': {
                'bot_id': 'test_id',
                'type': 'message',
                'text': "test_message",
                'user': 'test_user',
                'ts': '1574953111.001100',
                'team': 'test_team',
                'bot_profile': "test_profile",
                'blocks': [],
                'channel': 'test_channel',
                'event_ts': '1574953111.001100',
                'channel_type': 'channel'
            },
            'type': 'event_callback',
            'event_id': 'test_id',
            'event_time': 1574953111,
            'authed_users': ['test_user']
        }

    def test_is_this_type(self, example_user_message_event, example_bot_message_event):
        handler = UserMessageEventHandler(message_sender=MessageSender(client=MockSlackClient(token='test',
                                                                                              channels=['test'])),
                                          interpreter=SlackCommandInterpreter(),
                                          supported_commands=[MockCommand(name='test',
                                                                          description='test command')])
        assert handler.is_this_type(example_user_message_event) is True
        assert handler.is_this_type(example_bot_message_event) is False