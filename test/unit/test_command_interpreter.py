import pytest

from grey_rose.command_interpreter import SlackCommandInterpreter
from grey_rose.mocks import MockCommand


class TestCommandInterpreter:

    @pytest.fixture
    def example_command_one(self):
        return MockCommand('test1', 'test1 command')
    
    @pytest.fixture
    def example_command_two(self):
        return MockCommand('test2', 'test2 command')
    
    @pytest.fixture
    def example_command_interpreter(self):
        return SlackCommandInterpreter()

    def test_fetch_matching_command_with_match(self, example_command_interpreter, example_command_one, example_command_two):
        interpreter = example_command_interpreter
        commands = [example_command_one, example_command_two]
        fetched_command_one = interpreter.fetch_matching_command('!rooms test1', commands)
        fetched_command_two = interpreter.fetch_matching_command('!rooms test2', commands)
        assert fetched_command_one is example_command_one
        assert fetched_command_two is example_command_two

    def test_fetch_matching_command_without_match(self, example_command_interpreter, example_command_one):
        interpreter = example_command_interpreter
        commands = [example_command_one]
        with pytest.raises(UnsupportedCommandException):
            interpreter.fetch_matching_command('!rooms test2', commands)

    def test_parse_message_to_command_with_match(self, example_command_interpreter, example_command_one, example_command_two):
        interpreter = example_command_interpreter
        commands = [example_command_one, example_command_two]
        fetched_command_one = interpreter.parse_message_to_command('!rooms test1', commands)
        fetched_command_two = interpreter.parse_message_to_command('!rooms test2', commands)
        assert fetched_command_one is example_command_one
        assert fetched_command_two is example_command_two

    def test_parse_message_to_command_without_match(self, example_command_interpreter, example_command_one):
        interpreter = example_command_interpreter
        commands = [example_command_one]
        with pytest.raises(UnsupportedCommandException):
            interpreter.parse_message_to_command('!rooms test2', commands)

    def test_parse_message_to_command_without_command(self, example_command_interpreter, example_command_one):
        interpreter = example_command_interpreter
        commands = [example_command_one]
        with pytest.raises(MissingCommandException):
            interpreter.parse_message_to_command('!rooms', commands)

    def test_parse_message_to_command_when_not_command(self, example_command_interpreter, example_command_one):
        interpreter = example_command_interpreter
        commands = [example_command_one]
        fetched_command = interpreter.parse_message_to_command('not a command request', commands)
        assert fetched_command is None
