import pytest

from grey_rose.slack_helpers import MessageBuilder as mb
from grey_rose.slack_helpers import MessageSender
from grey_rose.mocks import MockSlackClient


class TestMessageBuilder:
    
    @pytest.fixture
    def example_markdown(self):
        return {'type': 'mrkdwn', 'text': 'test'}
    
    @pytest.fixture
    def example_section(self, example_markdown):
        return {'type': 'section', 'text': example_markdown}
    
    @pytest.fixture
    def example_single_block(self, example_section):
        return {'blocks': (example_section,)}

    @pytest.fixture
    def example_blocks(self, example_section):
        return {'blocks': (example_section, example_section, example_section)}

    def test_markdown(self, example_markdown):
        assert mb.markdown('test') == example_markdown

    def test_divider(self):
        expected = {'type': 'divider'}
        assert mb.divider() == expected

    def test_section(self, example_markdown, example_section):
        assert mb.section(example_markdown) == example_section

    def test_text_block(self, example_section):
        assert mb.text_block('test') == example_section
    
    def test_message(self, example_blocks, example_section): 
        assert mb.message(example_section, example_section, example_section) == example_blocks

    def test_text_message(self, example_single_block):
        assert mb.text_message('test') == example_single_block


class TestMessageSender:
    
    @pytest.fixture
    def message(self):
        return {
            'blocks': ({
                'type': 'section', 
                'text': {'type': 'mrkdwn', 'text': 'test'}
                },)
            }

    @pytest.fixture
    def example_channels(self):
        return ({'name': 'test_1', 'id': 'id_1', 'is_channel': True},
                {'name': 'test_2', 'id': 'id_2', 'is_channel': True},
                {'name': 'test_3', 'id': 'id_3', 'is_channel': False})

    @pytest.fixture
    def mock_client(self, example_channels):
        return MockSlackClient(token='test', channels=example_channels)

    @pytest.fixture
    def sender(self, mock_client):
        return MessageSender(mock_client)
    
    def test_default_channel_is_general(self, sender):
        assert sender.current_channel == 'general'

    def test_send(self, sender, message):
        output_message = sender.send(message)
        assert message['as_user'] is True
        assert message['ts'] == ''
        assert message['channel'] == sender.current_channel

    def test_get_channel_id_by_name_with_matches(self, sender):
        assert sender.get_channel_id_by_name('test_1') == 'id_1'
        assert sender.get_channel_id_by_name('test_2') == 'id_2'

    def test_get_channel_id_by_name_with_no_matches(self, sender):
        assert sender.get_channel_id_by_name('test_4') is None

    def test_get_channel_id_by_name_with_not_channel(self, sender):
        assert sender.get_channel_id_by_name('test_3') is None

    def test_switch_channels_with_channel_name(self, sender):
        sender.switch_channels(channel='test_1', is_channel_id=False)
        assert sender.current_channel == 'id_1'

    def test_switch_channels_with_channel_id(self, sender):
        sender.switch_channels(channel='id_2', is_channel_id=True)
        assert sender.current_channel == 'id_2'

